{ reflex-platform ? import ./deps/reflex-platform {} }:
let nixpkgs = reflex-platform.nixpkgs;
in nixpkgs.stdenv.mkDerivation (rec {
  name = "app";
  snaplets = ./app/snaplets;
  static = ./app/static;
  app = ./app;

  deps = ./deps;
  deploy = ./deploy;
  userbuild = ./userbuild;
  backend = import ./backend { nixpkgs = reflex-platform.nixpkgs; };
  frontend = import ./frontend { inherit reflex-platform; };
  develCfgTemplate = ./app/devel.cfg.template;
  buildEnv = (import ./userbuild/default.nix {}).env.nativeBuildInputs;
  myNixPkgs = nixpkgs.path;
  builder = builtins.toFile "builder.sh" ''
    source "$stdenv/setup"

    set -x

    mkdir -p "$out"

    ln -s "$myNixPkgs" "$out/nixpkgs"
    cp -r --no-preserve=mode "$static" "$out/static"
    cp -r --no-preserve=mode "$snaplets" "$out/snaplets"
    ln -s "$static" "$out/static"
    ln -s "$userbuild" "$out/userbuild-template"
    mkdir -p "$out/deps"
    ln -s "$deps/diagrams-reflex" "$out/deps/diagrams-reflex"
    ln -s "$deps/reflex-dom-contrib" "$out/deps/reflex-dom-contrib"
    ln -s "$deps/reflex-platform" "$out/deps/reflex-platform"
    echo "$buildEnv" >"$out/buildEnv"

    rm -f "$out/static/all.js"
    ln -s "$frontend/bin/frontend.jsexe/all.js" "$out/static"

    # closure-compiler -O ADVANCED --js_output_file="$out/static/all.js" # "$frontend/bin/frontend.jsexe/all.js"

    mkdir "$out/bin"
    ln -s "$backend/bin/main" "$out/bin"
    ln -s "$backend/bin/build-snippet" "$out/bin"
    ln -s "$deploy/setup-env.sh" "$out/bin/setup-env.sh"

    ln -s "$develCfgTemplate" "$out/devel.cfg.template"
  '';
  buildInputs = with nixpkgs; [
    closurecompiler
  ];
})

