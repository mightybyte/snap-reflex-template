<apply template="base">

  <ifLoggedIn>
    <div id="app-root" class="full-height">
    </div>

    <script src="/all.js"></script>

    <ignore>
    <apply template="mockup"/>
    </ignore>
  </ifLoggedIn>

  <ifLoggedOut>
    <apply template="_login"/>
  </ifLoggedOut>

</apply>
