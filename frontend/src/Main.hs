{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

module Main where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Trans
import           Control.Monad.Trans.Reader
import           Data.Char
import           GHCJS.DOM
import           GHCJS.DOM.Document (getElementById, getBody)
import           GHCJS.DOM.Element
import           GHCJS.DOM.EventM
import           GHCJS.DOM.HTMLDocument
import           GHCJS.DOM.HTMLElement
import           Reflex.Dom hiding (getKeyEvent)
import           Reflex.Dom.Contrib.KeyEvent
import           Reflex.Dom.Contrib.Utils
------------------------------------------------------------------------------
import           App
------------------------------------------------------------------------------


------------------------------------------------------------------------------
getRoot :: HTMLDocument -> String -> IO HTMLElement
getRoot doc appRootId = waitUntilJust $ liftM (fmap castToHTMLElement) $
                getElementById doc appRootId


------------------------------------------------------------------------------
main :: IO ()
main = runWebGUI $ \webView -> do
    doc <- waitUntilJust $ liftM (fmap castToHTMLDocument) $
      webViewGetDomDocument webView
    root <- getRoot doc "app-root"
    body <- waitUntilJust $ getBody doc
    attachWidget root webView $ do
      let eventTargetAbsorbsKeys = do
            Just t <- liftM (fmap castToHTMLElement) eventTarget
            Just n <- liftIO $ getTagName t
            return $ n `elem` ["INPUT", "SELECT", "TEXTAREA"]
      liftIO $ (`on` keyDown) body $ do
        ke <- getKeyEvent
        absorbs <- eventTargetAbsorbsKeys
        when (ke == (key $ chr 8) && not absorbs) preventDefault
      let wrapKeypress connectFunc = wrapDomEventMaybe body connectFunc $ do
            ke <- getKeyEvent
            absorbs <- eventTargetAbsorbsKeys
            return $ if absorbs && ke /= (key $ chr 27) -- Let the escape through
                     then Nothing
                     else Just ke
      kd <- wrapKeypress (`on` keyDown)
      kp <- wrapKeypress (`on` keyPress)
      runReaderT runApp $ AppState kd kp
