{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE RecursiveDo           #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module App where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Trans.Reader
import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Monoid
import           Reflex
import           Reflex.Dom
import           Reflex.Dom.Contrib.KeyEvent
import           Reflex.Dom.Contrib.Utils
import           Reflex.Dom.Contrib.Widgets.EditInPlace
------------------------------------------------------------------------------
import           Tabs
------------------------------------------------------------------------------


------------------------------------------------------------------------------
type App t m a = ReaderT (AppState t) m a


------------------------------------------------------------------------------
data AppState t = AppState
    { bsKeyDown :: Event t KeyEvent
    , bsKeyPress :: Event t KeyEvent
    }


runApp :: MonadWidget t m => App t m ()
runApp = do
    rec menu
        elAttr "div" ("class" =: "ui two column padded grid" <>
               "style" =: "height: calc(100% - 40px)") $ do
          text "Hello world"
    return ()

menu :: MonadWidget t m => m ()
menu = do
    divClass "ui blue inverted attached borderless menu" $ do
      elClass "span" "item active" $ text "Drinks I can make"
      elClass "span" "item" $ do
        rec title <- holdDyn "Untitled" titleEdits
            titleEdits <- editInPlace (constant True) title
        return ()
      divClass "right menu" $ do
        elAttr "a" ("class" =: "item" <> "href" =: "/logout") $
          text "Sign Out"

icon
    :: MonadWidget t m
    => String
    -> Dynamic t (Map String String)
    -> String
    -> m (Event t ())
icon nm attrs txt = do
    as <- mapDyn (M.insertWith (\n o -> unwords [o,n]) "class" "item") attrs
    (e,_) <- elDynAttr' "a" as $ do
      elClass "i" (unwords [nm, "icon"]) blank
      text txt
    return $ domEvent Click e

loading :: MonadWidget t m => m ()
loading = do
    divClass "ui segment full-height" $ do
      divClass "ui active dimmer full-height" $
        divClass "ui large text loader" $ text "Building..."
      replicateM_ 3 $ el "p" blank
